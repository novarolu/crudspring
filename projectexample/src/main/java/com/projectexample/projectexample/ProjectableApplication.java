package com.projectexample.projectexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class ProjectableApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectableApplication.class, args);
	}


}
