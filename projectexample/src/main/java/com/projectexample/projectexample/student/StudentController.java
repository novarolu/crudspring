package com.projectexample.projectexample.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/student/")
public class StudentController {
    private final StudentService studentservice;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentservice = studentService;
    }

    @GetMapping
    public List<Student> getStudents() {
        System.out.println(studentservice.getStudents());

        return studentservice.getStudents();
    }

    @PostMapping
    public void registerNewStudent(@RequestBody Student student) {

        studentservice.addNewStudent(student);
    }




    @DeleteMapping(value= "/delete/{studentId}" )
    public void  deleteStudent (@PathVariable("studentId") Long studentId){
        studentservice.deleteStudent(studentId);

    }

    @PutMapping(value= "/{studentId}", produces = "application/json" )
    //@PutMapping("/{studentId}")
    public void updateStudent(
            @PathVariable("studentId") Long studentId,
            @RequestParam(required = false) String name ,
            @RequestParam(required = false) String email ){
            studentservice.updateStudent(studentId,name,email);

        }



    }



