package com.projectexample.projectexample.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepositoty;

    @Autowired
    public StudentService(StudentRepository studentRepositoty) {
        this.studentRepositoty = studentRepositoty;
    }

    public List<Student> getStudents(){

    return studentRepositoty.findAll();


}

    public void addNewStudent(Student student) {
        System.out.println("es estudiante"+ student);
       Optional<Student> studentOptional= studentRepositoty
               .findStudentByEmail(student.getEmail());
       if(studentOptional.isPresent()){
           throw  new IllegalStateException("Email taken");
       }
        studentRepositoty.save(student);
    }

    public void deleteStudent(Long studentId) {
        System.out.println("Este es el mensaje");
        System.out.println(studentId);
     boolean exists= studentRepositoty.existsById(studentId);
     if(!exists){
         throw  new IllegalStateException("student with id" +studentId+ "does not exists");
     }
     studentRepositoty.deleteById(studentId);

    }

    @Transactional
    public void updateStudent(
            Long studentId,
            String name,
            String email) {
        Student student= studentRepositoty.findById(studentId)
                .orElseThrow(() -> new IllegalStateException(
                        "student with id" +studentId+ "Does not exist"));
        if(name !=null && name.length()>0 && !Objects.equals(student.getName(), name)){
            student.setName(name);
            System.out.println("valor llegoogoege");
        }
        if(email != null && email.length()>0 && !Objects.equals(student.getEmail(), email)){
            Optional<Student> studentOptional= studentRepositoty
                    .findStudentByEmail(email);
            if(studentOptional.isPresent()){
                throw  new IllegalStateException("Email take");
            }
            student.setEmail(email);
        }

    }
}
